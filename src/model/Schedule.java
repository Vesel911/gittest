package model;

import java.util.Date;

public class Schedule {

    public String name;
    public String id;
    public Date fromDate;
    public Date tillDate;
    public String remarks;
}
